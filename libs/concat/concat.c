#include "concat/concat.h"
#include <string.h>
#include <stdlib.h>

// each call of this function should be free()
char* concat_strings(char* rec_string, char* add_string) {
       char* temp_string = (char*)malloc(sizeof(char)*(strlen(rec_string) + strlen(add_string) + 1));
       strcpy(temp_string,rec_string);
       strcat(temp_string,add_string);
    return temp_string;
}
