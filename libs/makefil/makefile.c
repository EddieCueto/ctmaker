#include "makefil/makefile.h"
#include <stdlib.h>
#include <stdio.h>

char* lib_h_file = "/*\n*\n* This file provides prototypes\n*\n*/\n\nint proto(int);";

char* lib_c_file = "#include \"example\example.h\"\n\nint proto(int argument) {\n\treturn 0;\n}";

char* lib_m_file = "# This is the library Makefile";

char* main_h_file = "/*\n*\n* This file provides prototypes\n*\n*/";

char* main_c_file = "#include <stdio.h>\n\nint main(int argc, char** argv) {\n\tprintf(\"Hello world!\\n\");\n\treturn 0;\n}";

char* main_m_file = "# This is the source Makefile";

char* ext_m_file = "# This is the main Makefile\n\nbuild:\n\t$(CC) src/main.c";

char* read_me = "# Project created with ctmaker";

void create_file(const char* path, const char* string) {
    FILE *fp = fopen(path, "ab");
    if (fp != NULL)
    {
        fputs(string, fp);
        fclose(fp);
    }
}
