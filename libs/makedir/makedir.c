#include "makedir/makedir.h"
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

int
make_directory(char* path) {
    int rc;

    rc = mkdir(path, S_IRWXU);
    if (rc != 0 && errno != EEXIST)
    {
        perror("mkdir");
        exit(1);
    }
    if (rc != 0 && errno == EEXIST)
        printf("%s already exists.\n", path);

    return 0;
}

