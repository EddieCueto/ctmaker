#include "../libs/makefil/makefile.c"
#include "../libs/makedir/makedir.c"
#include "../libs/license/license.c"
#include "../libs/concat/concat.c"
#include <unistd.h>
#include <limits.h>
#include <stdio.h>

int main(int argc, char** argv) {
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working dir: %s\n", cwd);
    } else {
        perror("getcwd() error");
        return 1;
    }
    char* license;
    char* lic_type;
    char* author;
    int opt;
    while ((opt = getopt(argc, argv, "l:a:")) != -1) {
        switch (opt) {
            case 'l':
                lic_type = optarg;
                break;
            case 'a':
                author = optarg;
                break;
            default:
                printf("Help: -l{BSD2,BSD3,GPL,MIT}\n");
                printf("      -a\"Your name\"\n");
                return -1;
                break;
        }
    }
    make_directory(concat_strings(cwd, "/libs"));
    make_directory(concat_strings(cwd, "/libs/example"));
    create_file(concat_strings(cwd, "/libs/example/example.c"), lib_c_file);
    make_directory(concat_strings(cwd, "/libs/example/example"));
    create_file(concat_strings(cwd, "/libs/example/example/example.h"), lib_h_file);
    make_directory(concat_strings(cwd, "/regress"));
    make_directory(concat_strings(cwd, "/regress/unittests"));
    make_directory(concat_strings(cwd, "/src"));
    create_file(concat_strings(cwd, "/src/main.c"), main_c_file);
    create_file(concat_strings(cwd, "/src/main.h"), main_h_file);
    create_file(concat_strings(cwd, "/src/Makefile"), main_m_file);
    create_file(concat_strings(cwd, "/Makefile"), ext_m_file);
    license = build_license(lic_type, author, "ctmaker");
    create_file(concat_strings(cwd, "/LICENSE"),license);
    create_file(concat_strings(cwd, "/README.md"),read_me);
    free(license);
    exit(0);
}
