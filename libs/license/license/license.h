/*
 * This file provides the prototypes for LICENSE
 * generation.
 *
 */

extern char* bsd2_1;
extern char* bsd2_2;
extern char* bsd3_1;
extern char* bsd3_2;
extern char* gpl_1;
extern char* gpl_2;
extern char* gpl_3;
extern char* mit_1;
extern char* mit_2;

char* build_license(char*, char*, char*);
void write_license();
